package com.airpolymorphic.justplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.airpolymorphic.justplayer.providers.PlayBackProvider;

public class MainActivity extends AppCompatActivity implements PlayBackProvider {

    TextView defaultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        defaultTextView = findViewById(R.id.def);


    }

    @Override
    public void setCountFiles(String countFiles) {
        defaultTextView.setText(countFiles);
    }

    @Override
    public void setFolderName(String folderName) {
        //folderName.setText(folderName);
    }

    @Override
    public void setTrackName(String name) {

    }
}
