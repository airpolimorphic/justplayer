package com.airpolymorphic.justplayer.providers;

public interface PlayBackProvider {

    void setCountFiles(String countFiles);

    void setFolderName(String folderName);

    void setTrackName(String name);

    static String buildCountFilesMessage(int currentIndex, int totalFiles){
        return String.format("%d/%d", currentIndex, totalFiles);
    }
}
