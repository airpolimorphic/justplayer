package com.airpolymorphic.justplayer.core.sources;

public interface Track extends SystemSource, Groupable {

    /*
        demand contents
     */

    void setTrackLength(long trackLength);

    long getTrackLength();

    String getTrackLengthShown();

    void setUserLikeIt(boolean userLikeIt);

    boolean isUserLikeIt();

    void setLastListened(long timestamp);

    long getLastListened();

}
