package com.airpolymorphic.justplayer.core;

import com.airpolymorphic.justplayer.core.sources.Groupable;

public class PlayList<T extends Groupable> extends RepeatList<T> {

    private RepeatList<T> stash = new RepeatList<>();
    private State state = State.DEFAULT;

    public void activateGroup(int group) {
        stash.clear();
        String currentItemId = this.getCurrent().getId();
        stash.addAll(this);
        clear();
        for (T item : stash) {
            if (item.getRepeatGroups().contains(group)) add(item);
            checkIsActive(item, currentItemId);
        }
    }

    public void deactivateGroup() {
        String currentItemId = this.getCurrent().getId();
        clear();
        addAll(stash);
        stash.clear();
        for (T item : this) checkIsActive(item, currentItemId);
    }

    private void checkIsActive(T item, String currentItemId) {
        if (item.getId().equals(currentItemId)) activateItem(item);
    }

    private void activateItem(T item) {
        int cursor = indexOf(item);
        setCursor(cursor >= 0 ? cursor : 0);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public enum State {
        DEFAULT, CUSTOM, DEMAND
    }
}
