package com.airpolymorphic.justplayer.core;

import java.util.ArrayList;
import java.util.List;

public class RepeatList<T> extends ArrayList<T> {
    private int cursor = 0;

    public RepeatList() {
        super();
    }

    public RepeatList(List<T> sourceList) {
        super(sourceList);
    }

    public int getPosition(T value) {
        if (value == null) return -1;
        int position = 0;
        for (T value1 : this) {
            if (value.equals(value1)) {
                return position;
            }
            position++;
        }
        return -1;
    }

    public T getNext() {
        int prediction = cursor + 1;
        if (prediction >= size()) cursor = 0;
        else cursor = prediction;
        return get(cursor);
    }

    public T getPrevious() {
        int prediction = cursor - 1;
        if (prediction < 0) cursor = size() - 1;
        else cursor = prediction;
        return get(cursor);
    }

    public boolean isReachedLeftBorder() {
        return cursor == 0;
    }

    public int getCursor() {
        return cursor;
    }

    public void setCursor(int cursor) {
        this.cursor = cursor;
    }

    public void incrementCursor() {
        getNext();
    }

    public void decrementCursor() {
        getPrevious();
    }

    public T getCurrent() {
        return get(cursor);
    }

    public int getCursorShown() {
        return cursor + 1;
    }

    public boolean isReachedRightBorder() {
        return cursor == size() - 1;
    }

}
