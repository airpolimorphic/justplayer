package com.airpolymorphic.justplayer.core.sources;

public interface SystemSource {
    String getName();

    String getAbsolutePath();

    void delete();

    void rename(String name);

    String getFolderName();

    long getSize();

    String getSizeShown();

    SourceType getSourceType();
}
