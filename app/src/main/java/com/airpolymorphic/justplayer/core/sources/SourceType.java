package com.airpolymorphic.justplayer.core.sources;

public enum SourceType {
    LOCAL, REMOTE
}
