package com.airpolymorphic.justplayer.core.sources;

import java.util.List;

public interface Groupable {

    void setRepeatGroup(List<Integer> group);

    void addGroup(Integer group);

    List<Integer> getRepeatGroups();

    String getId();
}
