package com.airpolymorphic.justplayer.sources;

import com.airpolymorphic.justplayer.providers.PlayBackProvider;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class LocalScanner {

    private static final List<String> EXCEPTIONS = Collections.synchronizedList(
            new ArrayList<>(Arrays.asList("thumb", "thumbs", "sys", "android", "system", "systems", "proc", "task", "EvilPlayerThrash")));

    private static final List<String> FORMATS = Collections.synchronizedList(
            new ArrayList<>(Arrays.asList(".mp3", ".flac", ".wav", ".ogg", ".m4a", ".aac")));

    private PlayBackProvider provider;
    private String root; //Status.SD_CARD.getAbsolutePath()
    private AtomicInteger countFoundedTracks = new AtomicInteger(0);

    public void setProvider(PlayBackProvider provider) {
        this.provider = provider;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public List<File> scan(File... roots) {
        roots = moveMainFolderToTop(roots);
        Set<File> allRoots = new HashSet<>();
        if (roots != null) allRoots.addAll(Arrays.asList(roots));

        List<File> out = new ArrayList<>();
        for (File root : allRoots) {
            provider.setTrackName("Try to scan: " + root.getAbsolutePath());
            if (root == null) continue;
            if (root.isHidden() || !root.canRead()) {
                provider.setTrackName("For root: Hidden or cant be read" + root.getAbsolutePath());
                continue;
            }
            try {
                out.addAll(scan(root));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Collections.sort(out, (browserItem, t1) ->
                browserItem.getName().compareTo(t1.getName())
        );

//        BrowserReceivers.DEFAULT_RECEIVER.updateBrowserStack();
//        BrowserBehaviorManager.initBackGroundRecalculationFolders(rootsForBrowser);

        return out;
    }

    private Set<File> scan(File folder) {
        Set<File> outFiles = new HashSet<>();
        if (isAnyNull(folder, folder.listFiles())) return outFiles;

        for (File current : folder.listFiles()) {
            if (canSkip(current)) continue;
            if (current.isDirectory()) {
                if (current.getAbsolutePath().equals(root)) continue;

                File[] internalFiles = current.listFiles();

                if (canSkip(current, internalFiles)) continue;

                provider.setFolderName("Loading...");
                Set<File> items = scan(current);
                if (containsMp3(items)) {
                    outFiles.addAll(items);
                    outFiles.add(current);
                }
            } else if (isSupportedFormat(current.getName())) {
                outFiles.add(current);
                provider.setCountFiles(PlayBackProvider.buildCountFilesMessage(0, countFoundedTracks.incrementAndGet()));
            }
        }
        return outFiles;
    }

    private boolean isAnyNull(Object... objects) {
        for (Object object : objects) if (object == null) return true;
        return false;
    }

    private boolean canSkip(File file, File... internalFiles) {
        return internalFiles == null
                || file.isHidden()
                || !file.canRead()
                || !file.canWrite()
                || internalFiles.length == 0
                || containsIgnoreCase(file.getName());
    }

    private boolean canSkip(File file) {
        return file.getAbsolutePath().equals(root)
                || file == null
                || file.getName().startsWith(".");
    }

    private File[] moveMainFolderToTop(File[] roots) {
        File[] out = new File[roots.length];
        int index = 0;
        for (File root : roots) {
            if (root.getAbsolutePath().contains("Android")) {
                File result = root;
                if (!result.getName().equals("Android")) {
                    result = result.getParentFile();
                }
                result = result.getParentFile();
                out[index] = result;
            } else out[index] = root;
            index++;
        }
        return out;
    }

    private boolean containsIgnoreCase(String name) {
        for (String exception : EXCEPTIONS) if (name.equalsIgnoreCase(exception)) return true;
        return false;
    }

    private boolean containsMp3(Set<File> items) {
        for (File item : items) if (item.isFile()) return true;
        return false;
    }

    private boolean isSupportedFormat(String format) {
        format = getExtension(format);
        for (String allowed : FORMATS) if (format.equalsIgnoreCase(allowed)) return true;
        return false;
    }

    private String getExtension(String name) {
        return !name.contains(".") ? name : name.substring(name.lastIndexOf("."));
    }
}
