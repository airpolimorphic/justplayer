package com.airpolymorphic.justplayer.core;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class RepeatListTest {
    private RepeatList<String> repeatList = new RepeatList<>();

    private static final String testRecord1 = "testRecord1";
    private static final String testRecord2 = "testRecord2";
    private static final String testRecord3 = "testRecord3";

    @Before
    public void setup() {
        repeatList.add(testRecord1);
        repeatList.add(testRecord2);
        repeatList.add(testRecord3);
    }

    @Test
    public void getPositionTest() {
        Assert.assertEquals(1, repeatList.getPosition(testRecord2));
        Assert.assertEquals(0, repeatList.getPosition(testRecord1));
    }

    @Test
    public void getPositionOnEmptyListTest() {
        repeatList.clear();
        Assert.assertEquals(-1, repeatList.getPosition(testRecord2));
        reset();
    }

    @Test
    public void getPositionOnEmptyListNullValueTest() {
        repeatList.clear();
        Assert.assertEquals(-1, repeatList.getPosition(null));
        reset();
    }

    @Test
    public void getPositionNullTest() {
        Assert.assertEquals(-1, repeatList.getPosition(null));
        reset();
    }

    @Test
    public void getNextCycleTest() {
        Assert.assertEquals(testRecord2, repeatList.getNext());
        Assert.assertEquals(testRecord3, repeatList.getNext());
        Assert.assertEquals(testRecord1, repeatList.getNext());
        Assert.assertEquals(testRecord2, repeatList.getNext());
        reset();
    }

    @Test
    public void getPreviousCycleTest() {
        Assert.assertEquals(testRecord3, repeatList.getPrevious());
        Assert.assertEquals(testRecord2, repeatList.getPrevious());
        Assert.assertEquals(testRecord1, repeatList.getPrevious());
        Assert.assertEquals(testRecord3, repeatList.getPrevious());
        reset();
    }

    @Test
    public void getNextCycleWithNullTest() {
        RepeatList<String> exampleList = new RepeatList<>(repeatList);
        exampleList.add(null);
        Assert.assertEquals(testRecord2, exampleList.getNext());
        Assert.assertEquals(testRecord3, exampleList.getNext());
        Assert.assertNull(exampleList.getNext());
        Assert.assertEquals(testRecord1, exampleList.getNext());
        reset();
    }


    @Test
    public void getPreviousCycleWithNullTest() {
        RepeatList<String> exampleList = new RepeatList<>(repeatList);
        exampleList.add(null);
        Assert.assertNull(exampleList.getPrevious());
        Assert.assertEquals(testRecord3, exampleList.getPrevious());
        Assert.assertEquals(testRecord2, exampleList.getPrevious());
        Assert.assertEquals(testRecord1, exampleList.getPrevious());
        reset();
    }

    @Test
    public void getNextCycleOnEmptyListTest() {
        repeatList.clear();
        Exception e = null;
        try {
            repeatList.getNext();
        } catch (IndexOutOfBoundsException ex) {
            e = ex;
        }
        Assert.assertNotNull(e);
        reset();
    }

    @Test
    public void getPreviousCycleOnEmptyListTest() {
        repeatList.clear();
        Exception e = null;
        try {
            repeatList.getPrevious();
        } catch (IndexOutOfBoundsException ex) {
            e = ex;
        }
        Assert.assertNotNull(e);
        reset();
    }

    @Test
    public void isReachedLeftBorderTest(){
        Assert.assertTrue(repeatList.isReachedLeftBorder());
        reset();
    }

    @Test
    public void isReachedRightBorder(){
        Assert.assertFalse(repeatList.isReachedRightBorder());
        repeatList.getNext();
        repeatList.getNext();
        Assert.assertTrue(repeatList.isReachedRightBorder());
        reset();
    }

    @Test
    public void incrementCursorTest(){
        Assert.assertEquals(0, repeatList.getCursor());
        repeatList.incrementCursor();
        Assert.assertEquals(1, repeatList.getCursor());
        reset();
    }

    @Test
    public void decrementCursorTest(){
        Assert.assertEquals(0, repeatList.getCursor());
        repeatList.decrementCursor();
        Assert.assertEquals(2, repeatList.getCursor());
        reset();
    }

    @Test
    public void getCurrentTest(){
        Assert.assertEquals(testRecord1, repeatList.getCurrent());
        repeatList.getNext();
        Assert.assertEquals(testRecord2, repeatList.getCurrent());
        reset();
    }

    @Test
    public void getCursorShown(){
        Assert.assertEquals(1, repeatList.getCursorShown());
    }


    private void reset(){
        repeatList.clear();
        setup();
        repeatList.setCursor(0);
    }
}
