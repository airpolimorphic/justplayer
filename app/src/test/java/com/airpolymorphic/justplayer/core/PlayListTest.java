package com.airpolymorphic.justplayer.core;

import com.airpolymorphic.justplayer.core.sources.Groupable;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PlayListTest {
    private PlayList<TestTrack> playlist = new PlayList<>();
    private final static int GROUP_1 = 1;
    private final static int GROUP_2 = 2;
    private final static int GROUP_3 = 3;

    private final static String id1 = "1";
    private final static String id2 = "2";
    private final static String id3 = "3";
    private final static String id4 = "4";

    @Before
    public void setup() {
        playlist.clear();
        TestTrack track1 = new TestTrack();
        TestTrack track2 = new TestTrack();
        TestTrack track3 = new TestTrack();
        TestTrack track4 = new TestTrack();

        track1.addGroup(GROUP_1);
        track2.addGroup(GROUP_2);
        track3.addGroup(GROUP_2);
        track4.addGroup(GROUP_1);

        track1.addGroup(GROUP_3);
        track2.addGroup(GROUP_3);
        track3.addGroup(GROUP_3);

        track1.setId(id1);
        track2.setId(id2);
        track3.setId(id3);
        track4.setId(id4);

        playlist.add(track1);
        playlist.add(track2);
        playlist.add(track3);
        playlist.add(track4);
    }


    @Test
    public void activateFolderRepeatThenDeactivateGroupTest() {
        Assert.assertEquals(0, playlist.getCursor());
        Assert.assertEquals(4, playlist.size());
        playlist.getNext();
        playlist.activateGroup(GROUP_2);
        Assert.assertEquals(2, playlist.size());
        Assert.assertEquals(id2, playlist.get(0).getId());
        Assert.assertEquals(id3, playlist.get(1).getId());

        playlist.deactivateGroup();
        Assert.assertEquals(4, playlist.size());
        Assert.assertEquals(id1, playlist.get(0).getId());
        Assert.assertEquals(id2, playlist.get(1).getId());

    }

    @Test
    public void activateRootGroupTest() {
        Assert.assertEquals(0, playlist.getCursor());
        Assert.assertEquals(4, playlist.size());

        playlist.activateGroup(GROUP_3);

        Assert.assertEquals(3, playlist.size());
        Assert.assertEquals(id1, playlist.get(0).getId());
        Assert.assertEquals(id2, playlist.get(1).getId());
        Assert.assertEquals(id3, playlist.get(2).getId());

        playlist.deactivateGroup();
        Assert.assertEquals(4, playlist.size());
        Assert.assertEquals(id1, playlist.get(0).getId());
        Assert.assertEquals(id2, playlist.get(1).getId());
        Assert.assertEquals(id3, playlist.get(2).getId());
        Assert.assertEquals(id4, playlist.get(3).getId());
    }

}

class TestTrack implements Groupable {
    private List<Integer> groups = new ArrayList<>();
    private String id;


    @Override
    public void setRepeatGroup(List<Integer> group) {
        this.groups = group;
    }

    @Override
    public void addGroup(Integer group) {
        this.groups.add(group);
    }

    @Override
    public List<Integer> getRepeatGroups() {
        return groups;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
