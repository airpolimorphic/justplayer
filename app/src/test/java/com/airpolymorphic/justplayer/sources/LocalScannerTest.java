package com.airpolymorphic.justplayer.sources;

import com.airpolymorphic.justplayer.providers.PlayBackProvider;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LocalScannerTest {

    private File mockedFSEmpty;
    private File mockedFS;
    private PlayBackProvider provider = buildEmptyProvider();

    @Before
    public void setup() {
        mockedFSEmpty = buildMockFolderRoot("/local/storage/");
        String rootStr = "/local/storage/";
        File track1 = buildMockFile(rootStr + "track1.mp3");
        File track2 = buildMockFile(rootStr + "track2.mp3");
        File track3 = buildMockFile(rootStr + "track3.flac");
        File trash = buildMockFile(rootStr + "trash.bin");
        File internalTrack = buildMockFile(rootStr + "internal/track4.mp3");
        File internalFolder = buildMockFolder(rootStr + "internal/", internalTrack);
        mockedFS = buildMockFolderRoot(rootStr, internalFolder, trash, track3, track1, track2);

    }

    @Test
    public void testScanEmpty() {
        LocalScanner scanner = new LocalScanner();
        scanner.setProvider(provider);
        List<File> result = scanner.scan();
        assertNotNull(result);
        assertEquals(0, result.size());
        result = scanner.scan(mockedFSEmpty);
        assertEquals(0, result.size());
    }

    @Test
    public void testScan() {
        LocalScanner scanner = new LocalScanner();
        scanner.setProvider(provider);
        List<File> result = scanner.scan(mockedFS);
        assertNotNull(result);
        assertEquals(result.size(), 5);

        assertEquals(result.get(0).getName(), "internal");
        assertEquals(result.get(1).getName(), "track1.mp3");
        assertEquals(result.get(2).getName(), "track2.mp3");
        assertEquals(result.get(3).getName(), "track3.flac");
        assertEquals(result.get(4).getName(), "track4.mp3");
    }

    private File buildMockFile(String absoluteLocation) {
        File file = mock(File.class);
        File buffer = new File(absoluteLocation);
        when(file.getName()).thenReturn(buffer.getName());
        when(file.isDirectory()).thenReturn(false);
        when(file.isFile()).thenReturn(true);
        when(file.getAbsolutePath()).thenReturn(absoluteLocation);
        when(file.isHidden()).thenReturn(false);
        when(file.canRead()).thenReturn(true);
        when(file.canWrite()).thenReturn(true);
        return file;
    }

    private File buildMockFolderRoot(String absoluteLocation, File... containingList) {
        File file = mock(File.class);
        File buffer = new File(absoluteLocation);
        when(file.getName()).thenReturn(buffer.getName());
        when(file.isDirectory()).thenReturn(true);
        when(file.isFile()).thenReturn(false);
        when(file.getAbsolutePath()).thenReturn(absoluteLocation);
        when(file.isHidden()).thenReturn(false);
        when(file.canRead()).thenReturn(true);
        when(file.canWrite()).thenReturn(true);
        when(file.listFiles()).thenReturn(containingList);
        return file;
    }

    private File buildMockFolder(String absoluteLocation, File... containingList) {
        File file = mock(File.class);
        File buffer = new File(absoluteLocation);
        when(file.getName()).thenReturn(buffer.getName());
        when(file.isDirectory()).thenReturn(true);
        when(file.isFile()).thenReturn(false);
        when(file.getAbsolutePath()).thenReturn(absoluteLocation);
        when(file.isHidden()).thenReturn(false);
        when(file.canRead()).thenReturn(true);
        when(file.canWrite()).thenReturn(true);
        when(file.listFiles()).thenReturn(containingList);
        return file;
    }

    private PlayBackProvider buildEmptyProvider() {
        return new PlayBackProvider() {
            @Override
            public void setCountFiles(String countFiles) {

            }

            @Override
            public void setFolderName(String folderName) {

            }

            @Override
            public void setTrackName(String name) {

            }
        };
    }
}
